document.querySelectorAll(".list-items .list-item").forEach((item) => {
  item.addEventListener("click", function () {
    document
      .querySelectorAll(".list-items .list-item")
      .forEach((item) => item.classList.remove("active"));
    this.classList.add("active");
  });
});

// function changeContent(url) {
//     document.getElementById("change_content").setAttribute("src", url);
//   }

function changeBackground(url) {
  document.getElementById(
    "change_background"
  ).style.backgroundImage = `url(${url})`;
}

function animationImg(index) {
  switch (index) {
    case 1:
      changeBackground("/images/sneaker.png");
      break;
    case 2:
      changeBackground("/images/land.png");
      break;
    case 3:
      changeBackground("/images/gym.png");
      break;
    default:
      break;
  }
}

$("#sneaker").on("click", function () {
  $(".land").hide();
  $(".social").hide();
  $(".sneaker").fadeIn();
});
$("#land").on("click", function () {
  $(".sneaker").hide();
  $(".social").hide();
  $(".land").fadeIn();
});
$("#social").on("click", function () {
  $(".sneaker").hide();
  $(".land").hide();
  $(".social").fadeIn();
});
